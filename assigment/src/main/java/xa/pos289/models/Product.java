package xa.pos289.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import groovyjarjarantlr4.v4.runtime.misc.Nullable;

@Entity
@Table(name="product")

public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long Id;
	
	@ManyToOne
	@JoinColumn(name="varian_id", insertable=false, updatable=false)
	public Varian varian;
	
	@Column(name="varian_id")
	private long VarianId;
	
	@NotNull
	@Column(name="initial", length = 10, unique = true)
	private String Initial;
	
	@NotNull
	@Column(name="name", length = 50, unique = true)
	private String Name;
	
	@Nullable
	@Column(name="description", length = 500)
	private String Description;
	
	@NotNull
	@Column(name="price")
	private double Price;
	
	@NotNull
	@Column(name="stock")
	private int Stock;
	
	@NotNull
	@Column(name="active")
	private Boolean Active;
	
	@NotNull
	@Column(name="created_by", length = 50)
	private String CreatedBy;
	
	@NotNull
	@Column(name="created_date")
	private LocalDateTime CreatedDate;
	
	@Nullable
	@Column(name="modify_by", length = 50)
	private String ModifyBy;
	
	@Nullable
	@Column(name="modify_date")
	private LocalDateTime ModifyDate;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public Varian getVarian() {
		return varian;
	}

	public void setVarian(Varian varian) {
		this.varian = varian;
	}

	public long getVarianId() {
		return VarianId;
	}

	public void setVarianId(long varianId) {
		VarianId = varianId;
	}

	public String getInitial() {
		return Initial;
	}

	public void setInitial(String initial) {
		Initial = initial;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public int getStock() {
		return Stock;
	}

	public void setStock(int stock) {
		Stock = stock;
	}

	public Boolean getActive() {
		return Active;
	}

	public void setActive(Boolean active) {
		Active = active;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public LocalDateTime getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		CreatedDate = createdDate;
	}

	public String getModifyBy() {
		return ModifyBy;
	}

	public void setModifyBy(String modifyBy) {
		ModifyBy = modifyBy;
	}

	public LocalDateTime getModifyDate() {
		return ModifyDate;
	}

	public void setModifyDate(LocalDateTime modifyDate) {
		ModifyDate = modifyDate;
	}

}
