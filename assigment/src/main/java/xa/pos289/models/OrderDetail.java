package xa.pos289.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import groovyjarjarantlr4.v4.runtime.misc.Nullable;

@Entity
@Table(name="order_detail")
public class OrderDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long Id;
	
	@ManyToOne
	@JoinColumn(name="order_header_id", insertable=false, updatable=false)
	public OrderHeader orderheader;
	
	@Column(name="order_header_id")
	private long OrderHeaderId;
	
	@ManyToOne
	@JoinColumn(name="product_id", insertable=false, updatable=false)
	public Product product;
	
	@Column(name="product_id")
	private long ProductId;
	
	@NotNull
	@Column(name="quantity")
	private double Quantity;
	
	@NotNull
	@Column(name="price")
	private double Price;
	
	@NotNull
	@Column(name="active")
	private Boolean Active;
	
	@NotNull
	@Column(name="created_by", length = 50)
	private String CreatedBy;
	
	@NotNull
	@Column(name="created_date")
	private LocalDateTime CreatedDate;
	
	@Nullable
	@Column(name="modify_by", length = 50)
	private String ModifyBy;
	
	@Nullable
	@Column(name="modify_date")
	private LocalDateTime ModifyDate;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public OrderHeader getOrderheader() {
		return orderheader;
	}

	public void setOrderheader(OrderHeader orderheader) {
		this.orderheader = orderheader;
	}

	public long getOrderHeaderId() {
		return OrderHeaderId;
	}

	public void setOrderHeaderId(long orderHeaderId) {
		OrderHeaderId = orderHeaderId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getProductId() {
		return ProductId;
	}

	public void setProductId(long productId) {
		ProductId = productId;
	}

	public double getQuantity() {
		return Quantity;
	}

	public void setQuantity(double quantity) {
		Quantity = quantity;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public Boolean getActive() {
		return Active;
	}

	public void setActive(Boolean active) {
		Active = active;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public LocalDateTime getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		CreatedDate = createdDate;
	}

	public String getModifyBy() {
		return ModifyBy;
	}

	public void setModifyBy(String modifyBy) {
		ModifyBy = modifyBy;
	}

	public LocalDateTime getModifyDate() {
		return ModifyDate;
	}

	public void setModifyDate(LocalDateTime modifyDate) {
		ModifyDate = modifyDate;
	}

	
	


}
