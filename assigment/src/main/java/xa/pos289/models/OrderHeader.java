package xa.pos289.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import groovyjarjarantlr4.v4.runtime.misc.Nullable;

@Entity
@Table(name="order_header")

public class OrderHeader {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long Id;

	@NotNull
	@Column(name="reference", length = 30)
	private String Reference;
	
	@NotNull
	@Column(name="amount")
	private double Amount;
	
	@NotNull
	@Column(name="active")
	private Boolean Active;
	
	@NotNull
	@Column(name="created_by", length = 50)
	private String CreatedBy;
	
	@NotNull
	@Column(name="created_date")
	private LocalDateTime CreatedDate;
	
	@Nullable
	@Column(name="modify_by", length = 50)
	private String ModifyBy;
	
	
	@Nullable
	@Column(name="modify_date")
	private LocalDateTime ModifyDate;


	public long getId() {
		return Id;
	}


	public void setId(long id) {
		Id = id;
	}


	public String getReference() {
		return Reference;
	}


	public void setReference(String reference) {
		Reference = reference;
	}


	public double getAmount() {
		return Amount;
	}


	public void setAmount(double amount) {
		Amount = amount;
	}


	public Boolean getActive() {
		return Active;
	}


	public void setActive(Boolean active) {
		Active = active;
	}


	public String getCreatedBy() {
		return CreatedBy;
	}


	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}


	public LocalDateTime getCreatedDate() {
		return CreatedDate;
	}


	public void setCreatedDate(LocalDateTime createdDate) {
		CreatedDate = createdDate;
	}


	public String getModifyBy() {
		return ModifyBy;
	}


	public void setModifyBy(String modifyBy) {
		ModifyBy = modifyBy;
	}


	public LocalDateTime getModifyDate() {
		return ModifyDate;
	}


	public void setModifyDate(LocalDateTime modifyDate) {
		ModifyDate = modifyDate;
	}

	
}
