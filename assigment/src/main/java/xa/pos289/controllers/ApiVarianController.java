package xa.pos289.controllers;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xa.pos289.models.Varian;
import xa.pos289.repositories.VarianRepo;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api")
public class ApiVarianController {
	
	@Autowired VarianRepo varrepo;
	
	@GetMapping("/varian")
	public ResponseEntity<Stream<Varian>> getAllVar(){
		try {
			List<Varian> varian = this.varrepo.findAll();
			Stream<Varian> stream = varian.stream();
			return new ResponseEntity<>(stream, HttpStatus.OK);
		}
		catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("varian/{id}")
	public ResponseEntity<Varian> getVarById(@PathVariable Long id){
		try {
			Varian varian = this.varrepo.findById(id).orElse(null);
			return new ResponseEntity<Varian>(varian,HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<Varian>(HttpStatus.NO_CONTENT);
			
		}
	}
	
	@PostMapping("/insertVarian")
	public ResponseEntity<Varian> insertVarian(@RequestBody Varian varian) {
		try {
			this.varrepo.save(varian);
//			Category mCategory = this.catrepo.findById(category.getId()).
			return new ResponseEntity<Varian>(varian,HttpStatus.OK);
		} 
		catch(Exception e) {
			return new ResponseEntity<Varian>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/editVarian/{id}")
	public ResponseEntity<Varian>editVarian(@RequestBody Varian varian, 
			@PathVariable Long id){
		try {
			varian.setId(id);
			this.varrepo.save(varian);
			return new ResponseEntity<Varian>(varian,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<Varian>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/deleteVarian/{id}")
	public ResponseEntity<Varian> deleteVarian(@PathVariable Long id){
		try {
			this.varrepo.deleteById(id);
			return new ResponseEntity<Varian>(HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<Varian>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/getvarbycatid/{id}")
	public ResponseEntity<?> getvarbycatid(@PathVariable("id") Long id){
		try {
			List<Varian> varian = this.varrepo.findByCategory_id(id);
			if(varian != null) {
				return new ResponseEntity<>(varian,HttpStatus.OK);
			}
			else {
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND)
						.body("Category ID dengan Id "+id+" tidak ditemukan");
			}
		} 
		catch (Exception e) {
			return new ResponseEntity<Varian>(HttpStatus.NO_CONTENT);
			
		}
	}
	
}
