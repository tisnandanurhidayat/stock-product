package xa.pos289.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.lowagie.text.DocumentException;

import xa.pos289.exporters.ProductExporter;
import xa.pos289.exporters.VariantExporter;
import xa.pos289.models.Category;
import xa.pos289.models.Product;
import xa.pos289.models.Varian;
import xa.pos289.repositories.CategoryRepo;
import xa.pos289.repositories.VarianRepo;
import xa.pos289.services.VarianServices;

@Controller
@RequestMapping(value="/varian/")

public class VarianController {
	
	@Autowired 	
	private VarianRepo varrepo;
	@Autowired
	private CategoryRepo catrepo;
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView ("/varian/index");
		List<Varian> varian = this.varrepo.findAll();
		view.addObject("varian", varian);
		return view;
	}	

	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view  = new ModelAndView("/varian/form");
		Varian varian = new Varian();
		view.addObject("varian", varian);
		List<Category> category = this.catrepo.findAll();
		view.addObject("category", category);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute Varian varian, BindingResult result) {
		if (!result.hasErrors()) {
			this.varrepo.save(varian);
		}
		return new ModelAndView("redirect:/varian/index");
	}
	
	@GetMapping(value="/edit/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view  = new ModelAndView("/varian/form");
		Varian varian = this.varrepo.findById(id).orElse(null);
		view.addObject("varian", varian);
		List<Category> category = this.catrepo.findAll();
		view.addObject("category", category);
		return view;
		
	}
	
	@GetMapping(value="/deleteform/{id}")
	public ModelAndView deleteform(@PathVariable("id") Long id) {
		ModelAndView view  = new ModelAndView("/varian/deleteform");
		Varian varian = this.varrepo.findById(id).orElse(null);
		view.addObject("varian", varian);
		return view;
	}
	
	@GetMapping(value="/del/{id}")
	public ModelAndView del(@PathVariable("id") Long id) {
		if (id != null) {
			this.varrepo.deleteById(id);
		}
		return new ModelAndView("redirect:/varian/index");
	}
	@Autowired VarianServices service;
	
	@GetMapping(value="exportvariant")
	public void exportvariant(HttpServletResponse response) throws DocumentException, IOException {
		response.setContentType("aplication/pdf");
		DateFormat dateformat = new SimpleDateFormat("yyyymmddHHmmss");
		String currendate = dateformat.format(new Date());
		
		String headerkey = "Content-Disposition";
		String headerValue = "attachment; filename = Variant_"+currendate+".pdf";
		response.setHeader(headerkey, headerValue);
		
		List<Varian> listvarian = service.listAll();
		
		VariantExporter exporter = new VariantExporter(listvarian);
		exporter.export(response);
	}
	
	

	
	

}
