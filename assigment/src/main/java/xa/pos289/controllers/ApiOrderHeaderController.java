package xa.pos289.controllers;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xa.pos289.models.OrderDetail;
import xa.pos289.models.OrderHeader;
import xa.pos289.repositories.OrderHeaderRepo;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api/")
public class ApiOrderHeaderController {
	
	@Autowired OrderHeaderRepo ohrepo;
	
	@GetMapping("/orderheader")
	public ResponseEntity<Stream<OrderHeader>> getAllOrderHeader(){
		try {
			List<OrderHeader> orderheader = this.ohrepo.findAll();
			Stream<OrderHeader> stream = orderheader.stream();
			return new ResponseEntity<Stream<OrderHeader>>(stream, HttpStatus.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<Stream<OrderHeader>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/orderheader/{id}")
	public ResponseEntity<?> getOrderHeaderById(@PathVariable Long id){
		try {
			OrderHeader orderheader = this.ohrepo.findById(id).orElse(null);
			if(orderheader != null) {
				return new ResponseEntity<OrderHeader>(orderheader,HttpStatus.OK);
			}
			else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("Order Header dengan ID "+ id +" tidak ditemukan");
			}
		} 
		catch (Exception e) {
			return new ResponseEntity<OrderHeader>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/orderheader")
	public ResponseEntity<OrderHeader> insertorderheader(@RequestBody OrderHeader orderheader){
		OrderHeader orderheaderData = this.ohrepo.save(orderheader);
		if(orderheaderData.equals(orderheader)) {
			return new ResponseEntity<OrderHeader>(orderheader,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<OrderHeader>(HttpStatus.NOT_FOUND);
		}
	}
	@PutMapping("/orderheader/{id}")
	public ResponseEntity<OrderHeader>editOrderHeader(@RequestBody OrderHeader orderheader, 
			@PathVariable Long id){
		try {
			orderheader.setId(id);
			this.ohrepo.save(orderheader);
			return new ResponseEntity<OrderHeader>(orderheader,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<OrderHeader>(HttpStatus.NO_CONTENT);
		}
	}
	
	

}
