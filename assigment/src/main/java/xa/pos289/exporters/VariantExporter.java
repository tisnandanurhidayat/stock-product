package xa.pos289.exporters;

import java.awt.Color;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import xa.pos289.models.Varian;

public class VariantExporter {
	
	private List<Varian> listvarian;
	
	public VariantExporter(List<Varian> listvarian) {
		this.listvarian = listvarian;
	}
	
	private void writeTableHeader(PdfPTable table) {
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.GRAY);
		cell.setPadding(6);
		
		Font font = FontFactory.getFont(FontFactory.TIMES_BOLDITALIC);
		font.setColor(Color.WHITE);
		
		cell.setPhrase(new Phrase("Category", font));
		table.addCell(cell);
		cell.setPhrase(new Phrase("Name", font));
		table.addCell(cell);
		cell.setPhrase(new Phrase("Initial", font));
		table.addCell(cell);
		cell.setPhrase(new Phrase("Active", font));
		table.addCell(cell);		
	}
	
	public void writeTableData(PdfPTable table) {
		for(Varian varian:listvarian) {
			table.addCell(String.valueOf(varian.getCategory().getName()));
			table.addCell(String.valueOf(varian.getName()));
			table.addCell(String.valueOf(varian.getInitial()));
			table.addCell(String.valueOf(varian.getActive()));
		}
	}
	
	public void export(HttpServletResponse response) throws DocumentException, IOException{
		Document document = new Document(PageSize.A4);
		PdfWriter.getInstance(document, response.getOutputStream());
		
		document.open();
		Font font = FontFactory.getFont(FontFactory.TIMES);
		font.setSize(16);
		font.setColor(Color.black);
		
		Paragraph p = new Paragraph("List Of Variant", font);
		p.setAlignment(Paragraph.ALIGN_CENTER);
		document.add(p);
		
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100f);
		table.setWidths(new float[] {2.5f, 2.5f, 2.5f, 2.5f,});
		table.setSpacingBefore(10);
		
		writeTableHeader(table);
		writeTableData(table);
		
		document.add(table);
		document.close();
		
		
	}
	

}
