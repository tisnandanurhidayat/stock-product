package xa.ioctest.configurations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import xa.ioctest.classes.HelloWorld;

@Configuration
public class Application {
	@Bean
	public HelloWorld helloWorld() {
		HelloWorld helloword = new HelloWorld();
//        Scanner input = new Scanner (System.in);
//        String sebelum , sesudah ="";
//        System.out.println("masukan kata atau kalimat :");
//        sebelum = input.nextLine();
//        int length = sebelum.length();
//        
//        for (int i = length -1; i >=0 ; i--) {
//			sesudah = sesudah + sebelum.charAt(i);
//
//		}
//		if (sebelum.equals(sesudah)) {
//			helloword.setMessage(sebelum + " adalah palindrom");
//		} else {
//			helloword.setMessage(sebelum +" bukan palindrom");
//		}

		List<String> names = Arrays.asList("Makanan", "Minuman", "Coklat", "jelly");
		List<String> result = names.stream().filter(s -> s.startsWith("C")).collect(Collectors.toList());
		helloword.setMessage(result);
		return helloword;
	}
}
